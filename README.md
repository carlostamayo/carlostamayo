# Jorge Carlos Tamayo

# Portfolio projects

- [ ] [NesJS REST API](https://gitlab.com/portfolio9753849/nestjs-api)
- [ ] [Express REST API with mongoose/MongoDB](https://gitlab.com/portfolio9753849/express-mongoose)
- [ ] [Next.js admin panel](https://github.com/JC-Tamayo/portfolio-nextjs-admin-panel)

